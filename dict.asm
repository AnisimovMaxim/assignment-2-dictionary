%include "lib.inc"

section .text

%define JUMP_KEY 8
global find_word
; Функция find_word принимает на вход два аргумента: 
; rsi - Указатель на начало словаря
; rdi - Указатель на нуль-терминированную строку
; Если найдено подходящее вхождение, то идёт возврат адреса начала вхождения в словарь, иначе вернёт 0
find_word:
	push r12
	push r13
	mov r12, rsi
	mov r13, rdi
.iteration:
	mov rdi, r13
	mov rsi, r12
	add rsi, JUMP_KEY
	call string_equals
	test rax, rax
	jnz .found_word
	test r12, r12
	je .not_found_word
	mov r12, [r12]
	jmp .iteration

.found_word:
	mov rax, r12
	pop r13
	pop r12
	ret
.not_found_word:
	xor rax, rax
	pop r13
	pop r12
	ret


