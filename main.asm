%include "lib.inc"
%include "colon.inc"
%include "words.inc"

extern find_word

%define BUFFER_SIZE 255
%define POINTER_SIZE 8
%define STDOUT_DESCRIPTOR 1

section .bss
buffer: resb (BUFFER_SIZE + 1)

section .rodata
INPUT_STRING_FAULT: db "Error in reading stdin!", 0
NOT_FOUND_STRING_FAULT: db "Error in finding this key!", 0

section .text
global _start:

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .input_error

	mov rdi, buffer
	mov rsi, KEY_WORD
	call find_word

	test rax, rax
	jz .find_error

	mov rdi, rax
	add rdi, POINTER_SIZE
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	call print_newline
	xor rdi, rdi
	jmp exit

	.input_error:
		mov rdi, INPUT_STRING_FAULT
		call print_error
		mov rdi, STDOUT_DESCRIPTOR
		jmp exit
	.find_error:
		mov rdi, NOT_FOUND_STRING_FAULT
		call print_error
		mov rdi, STDOUT_DESCRIPTOR
		jmp exit
