ASM=nasm
ASMFLAGS=-felf64
LD=ld

.PHONY: all test clean

all: program

main.o: main.asm words.inc lib.inc dict.inc

words.inc: colon.inc

dict.o: dict.asm lib.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

program: dict.o main.o lib.o
	$(LD) -o $@ $^


clean:
	rm -f *.o
	rm -f program

test: 
	python test.py

