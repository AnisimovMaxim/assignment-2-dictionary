import subprocess

input_string = ["group_name", "name"," ", "surname", "efesfq"]

expected_output = ["P3233", "Maxim", " ", "Anisimov", " "]

expected_error = [" ", " ", "Error in reading stdin!", " ", "Error in finding this key!"]

errors = True
for i in range(len(input_string)):
	proc = subprocess.Popen(["./program"],stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate(input=input_string[i].encode())
	stdout = stdout.decode().strip()
	stderr = stderr.decode().strip()
	if len(stdout)==0:
		stdout=" "
	if len(stderr)==0:
		stderr=" "
	if stdout == expected_output[i] and stderr == expected_error[i]:
		print("Test "+str(i+1)+": passed")
	else:
		print("Test "+str(i+1)+": failed")
		if stdout != expected_output[i]:
			errors = False
			print("Expected in stdout: "+expected_output[i]+", but print: "+stdout+".")
		if stderr != expected_error[i]:
			errors = False
			print("Expected in stderr: "+expected_error[i]+", but print: "+stderr+".")
	print("\n")

if errors:
	print("All tests are passed")



