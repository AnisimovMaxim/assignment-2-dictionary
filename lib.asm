section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global string_copy
global read_char
global read_word
global parse_uint
global parse_int
global print_error

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax

    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop

    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
        mov rsi, rdi
        mov rdx, rax
        mov rdi, 1
    mov rax, 1
    
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
        mov rsi, rsp
    mov rdx, 1
    mov rax, 1
        mov rdi, 1
   
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
        jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 10
    mov r10, rsp
    mov rax, rdi

    sub rsp, 32      ; align the stack to a 16-byte boundary

        dec r10
        mov byte [r10], 0
        .loop:
            xor rdx, rdx
                div rcx
                add dl, '0'
                dec r10
                mov byte [r10], dl
                test rax, rax
                jnz .loop
        mov rdi, r10
        call print_string
        add rsp, 32       ; restore the stack

        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        test rdi, rdi
        jge .positive_case

        neg rdi
            push rdi
        mov rdi, '-'
        call print_char
        pop rdi

        .positive_case:
                jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r9b, byte [rdi]
                cmp r9b, byte [rsi]
        jne .zero_case
                
        test r9b, r9b
        je .one_case
                inc rdi
        inc rsi     
                jmp .loop
    .one_case:
        mov rax, 1
        ret
    .zero_case:
        mov rax, 0
        ret
        

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        mov rax, 0
        push 0
        mov rdi, 0
        mov rsi, rsp
        mov rdx, 1

        syscall
        pop rax
           ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rsi
    mov r13, rdi
    
        .skip_spaces:
        xor r14, r14
        call read_char
        cmp rax, 0x20
        jz .skip_spaces
        cmp rax, 0x9
        jz .skip_spaces
        cmp rax, 0xA
        jz .skip_spaces
        cmp rax, 0
        jz .not_complete
        jmp .write
    
        .read:
        call read_char
        cmp rax, 0
        jz .complete
                cmp rax, 0x20
        jz .complete
                cmp rax, 0x9
        jz .complete
                cmp rax, 0xA
        jz .complete
    
        .write:
        mov byte[r13 + r14], al
        inc r14
        cmp r14, r12 
        jz .not_complete
        jmp .read
    
        .complete:
        mov byte[r13 + r14], 0
        mov rax, r13
        mov rdx, r14
        jmp .end
    
        .not_complete:
        xor rax, rax
        xor rdx, rdx
        jmp .end
    
        .end:
        pop r14
        pop r13
        pop r12
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor     rax, rax
        xor     rcx, rcx
        xor     rdx, rdx

        .loop:
                mov     al, [rdi + rcx]
        test al, al
                jz      .end
                sub al, '0'             
                js      .end
                cmp     al, 9
                ja      .end
                inc     rcx
                imul rdx, 10
                add     dl, al
                jmp     .loop
        .end:
                mov     rax, rdx
                mov     rdx, rcx
                ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        mov     al, [rdi]
        cmp     al, '-'
        je      .negative_case
                call parse_uint
                ret
        .negative_case:
                inc     rdi
                push rdi
                call parse_uint
                pop rdi
                neg     rax
                inc     rdx
                ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
        xor r9, r9
    .loop:
        cmp rax, rdx
                jge .not_complete
                mov r9b, byte [rdi + rax]
                mov byte [rsi + rax], r9b
                test r9b, r9b
                jz .complete
                inc rax
                jmp .loop
        .not_complete:
                xor rax, rax
        .complete:
                ret

global print_error
; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret
